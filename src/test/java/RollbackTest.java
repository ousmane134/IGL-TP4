import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.sql.*;

public class RollbackTest {
	Connexion con;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		con = new Connexion("local","//postgres:5432/postgres","postgres","");
		con.setAutoCommit(false);
		try {
			String query = "CREATE TABLE Ligue(\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL,\n" +
					"\tCONSTRAINT primary_key_ligue PRIMARY KEY(nomLigue) \n" +
					");\n" +
					"\n" +
					"CREATE TABLE Equipe(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL,\n" +
					"\tNbreJoueurMax INTEGER NOT NULL,\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL REFERENCES Ligue,\n" +
					"\tCONSTRAINT primary_key_equipe PRIMARY KEY(nomEquipe)\n" +
					");\n" +
					"\n" +
					"CREATE TABLE Participant(\n" +
					"\tmatricule\tCHAR(8) NOT NULL,\n" +
					"\tage INTEGER NOT NULL,\n" +
					"\tnom\tVARCHAR(255) NOT NULL ,\n" +
					"\tprenom\tVARCHAR(255) NOT NULL ,\n" +
					"\tCONSTRAINT primary_key_participant PRIMARY KEY(matricule)\n" +
					");\n" +
					"\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Joueur(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL REFERENCES Equipe,\n" +
					"\tmatricule CHAR(8) NOT NULL REFERENCES Participant\n" +
					");\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Resultat(\n" +
					"\tId\tINTEGER NOT NULL,\n" +
					"\tscoreEquipeA INTEGER NOT NULL,\n" +
					"\tscoreEquipeB INTEGER NOT NULL,\n" +
					"\tnomEquipeA\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tnomEquipeB\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tCONSTRAINT primary_key_resultat PRIMARY KEY(Id)\n" +
					");";
			Statement stmt = con.getConnection().createStatement();
			stmt.executeUpdate(query);
			con.commit();
		} catch (Exception e) {
			System.err.println("Failed to Execute  scriptCreation. The error is " + e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
		//Destruction des tables dans la base de données
		try {
			String query = "DROP TABLE Participant CASCADE;\n" +
					"DROP TABLE Equipe CASCADE;\n" +
					"DROP TABLE Joueur CASCADE;\n" +
					"DROP TABLE Ligue CASCADE;\n" +
					"DROP TABLE Resultat CASCADE;";
			Statement stmt = con.getConnection().createStatement();
			stmt.executeUpdate(query);
			con.commit();

		} catch (Exception e) {
			System.err.println("Failed to Executes scriptdestruction. The error is " + e.getMessage());
		}
		con.fermer();
	}

	@Test
	public void test() throws Exception{
		String query = "INSERT INTO Ligue (nomLigue) VALUES ('Quidditch')";
		Statement stmt = con.getConnection().createStatement();
		stmt.executeUpdate(query);
		PreparedStatement stmtAfficherLigue= con.getConnection().prepareStatement("SELECT nomLigue FROM Ligue");
		ResultSet rset = stmtAfficherLigue.executeQuery();
		System.out.println(rset.next());
		con.rollback();
		System.out.println(rset.next());
		assertFalse(rset.next());
		System.out.println("Test réussi, rollback.");
	}
}
