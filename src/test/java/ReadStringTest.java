import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.StringTokenizer;

public class ReadStringTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		Devoir2 devoir = new Devoir2();
		String result;
		StringTokenizer token = new StringTokenizer("testTest");
		try{
			result = devoir.readString(token);
		}catch(Exception e){
			throw e;
		}
		assertEquals("testTest",result);
		System.out.println("Test réussi: readstring");
	}
}
