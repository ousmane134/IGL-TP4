import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.StringTokenizer;

public class ReadDateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void test() throws Exception {
		Devoir2 devoir = new Devoir2();
		Date result;
		StringTokenizer token = new StringTokenizer("2011-12-21");
		try{
			result = devoir.readDate(token);
		}catch(Exception e){
			throw e;
		}
		assertEquals("2011-12-21",result.toString());
		System.out.println("Test réussi: readdate");
	}
}
