import org.junit.*;
import java.io.*;
import java.sql.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FonctionnelAjoutSuppAffLigueTest {

	Connection con;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Driver d;
		d = (Driver)Class.forName("org.postgresql.Driver").newInstance();
		DriverManager.registerDriver(d);
		con= DriverManager.getConnection("jdbc:postgresql://postgres:5432/postgres", "postgres", "");


		//Création des tables dans la base de données
		try {
			String query = "CREATE TABLE Ligue(\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL,\n" +
					"\tCONSTRAINT primary_key_ligue PRIMARY KEY(nomLigue) \n" +
					");\n" +
					"\n" +
					"CREATE TABLE Equipe(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL,\n" +
					"\tNbreJoueurMax INTEGER NOT NULL,\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL REFERENCES Ligue,\n" +
					"\tCONSTRAINT primary_key_equipe PRIMARY KEY(nomEquipe)\n" +
					");\n" +
					"\n" +
					"CREATE TABLE Participant(\n" +
					"\tmatricule\tCHAR(8) NOT NULL,\n" +
					"\tage INTEGER NOT NULL,\n" +
					"\tnom\tVARCHAR(255) NOT NULL ,\n" +
					"\tprenom\tVARCHAR(255) NOT NULL ,\n" +
					"\tCONSTRAINT primary_key_participant PRIMARY KEY(matricule)\n" +
					");\n" +
					"\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Joueur(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL REFERENCES Equipe,\n" +
					"\tmatricule CHAR(8) NOT NULL REFERENCES Participant\n" +
					");\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Resultat(\n" +
					"\tId\tINTEGER NOT NULL,\n" +
					"\tscoreEquipeA INTEGER NOT NULL,\n" +
					"\tscoreEquipeB INTEGER NOT NULL,\n" +
					"\tnomEquipeA\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tnomEquipeB\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tCONSTRAINT primary_key_resultat PRIMARY KEY(Id)\n" +
					");";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.err.println("Failed to Execute  scriptCreation. The error is " + e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {

		//Destruction des tables dans la base de données
		try {
			String query = "DROP TABLE Participant CASCADE;\n" +
					"DROP TABLE Equipe CASCADE;\n" +
					"DROP TABLE Joueur CASCADE;\n" +
					"DROP TABLE Ligue CASCADE;\n" +
					"DROP TABLE Resultat CASCADE;";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.err.println("Failed to Executes criptdestruction. The error is " + e.getMessage());
		}
		con.close();
	}


	//Ce test ajoute deux ligue dans la base de données, ferme l'application
	//Par la suite, rajoute une équipe et ferme a nouveau l'application
	//Par la suite, supprimer l'equipe et une ligue
	@Test
	public void test() throws Exception{
		//Démarrage de l'application
		Devoir2 devoir = new Devoir2();
		String args[] = {"local", "//postgres:5432/postgres", "postgres", ""};

		//Ajout des 2 ligues
		String input = "ajoutLigue Quidditch\n ajoutLigue Soccer\n";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		try{
			devoir.main(args);
		}catch(Exception e){
			throw e;
		}

		//Ajout de l'equipe
		input = "ajoutEquipe Quidditch Griffondor 7";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		try{
			devoir.main(args);
		}catch(Exception e){
			throw e;
		}

		//Suppression de l'equipe et d'un ligue, supprimer une ligue contenant des equipes supprime egalement les equipes
		input = "supprimerLigue Quidditch";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		try{
			devoir.main(args);
		}catch(Exception e){
			throw e;
		}


		//Execution d'une commande SELECT dans la base de données
		PreparedStatement stmtAfficherLigue= con.prepareStatement("SELECT nomLigue FROM Ligue", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet rset = stmtAfficherLigue.executeQuery();

		if(!rset.next())
			fail("Échec, aucune ligue trouvée.");

		if(rset.first()==rset.last()){
			assertEquals("Soccer", rset.getString("nomLigue"));
		}
		else
			fail("Échec, une ligue n'a pas été supprimée.");


		PreparedStatement stmtAfficherLigue2= con.prepareStatement("SELECT nomEquipe FROM Equipe", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet rset2 = stmtAfficherLigue2.executeQuery();

		if(!rset.next()){
			System.out.println("Test réussi! (Fonctionnel: Ajout-Supprimer ligue)\n");
		}
		else
			fail("Échec, aucune ligue trouvée.");

	}

}
