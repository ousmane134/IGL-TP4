import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;

public class SupprimerJoueurTest {
	Connection con;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Driver d;
		d = (Driver)Class.forName("org.postgresql.Driver").newInstance();
		DriverManager.registerDriver(d);
		con= DriverManager.getConnection("jdbc:postgresql://postgres:5432/postgres", "postgres", "");

		//Création des tables dans la base de données
		try {

			String query = "CREATE TABLE Ligue(\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL,\n" +
					"\tCONSTRAINT primary_key_ligue PRIMARY KEY(nomLigue) \n" +
					");\n" +
					"\n" +
					"CREATE TABLE Equipe(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL,\n" +
					"\tNbreJoueurMax INTEGER NOT NULL,\n" +
					"\tnomLigue\tVARCHAR(255) NOT NULL REFERENCES Ligue,\n" +
					"\tCONSTRAINT primary_key_equipe PRIMARY KEY(nomEquipe)\n" +
					");\n" +
					"\n" +
					"CREATE TABLE Participant(\n" +
					"\tmatricule\tCHAR(8) NOT NULL,\n" +
					"\tage INTEGER NOT NULL,\n" +
					"\tnom\tVARCHAR(255) NOT NULL ,\n" +
					"\tprenom\tVARCHAR(255) NOT NULL ,\n" +
					"\tCONSTRAINT primary_key_participant PRIMARY KEY(matricule)\n" +
					");\n" +
					"\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Joueur(\n" +
					"\tnomEquipe\tVARCHAR(255) NOT NULL REFERENCES Equipe,\n" +
					"\tmatricule CHAR(8) NOT NULL REFERENCES Participant\n" +
					");\n" +
					"\n" +
					"\n" +
					"CREATE TABLE Resultat(\n" +
					"\tId\tINTEGER NOT NULL,\n" +
					"\tscoreEquipeA INTEGER NOT NULL,\n" +
					"\tscoreEquipeB INTEGER NOT NULL,\n" +
					"\tnomEquipeA\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tnomEquipeB\tVARCHAR(255) NOT NULL REFERENCES Equipe(nomEquipe) ON DELETE CASCADE,\n" +
					"\tCONSTRAINT primary_key_resultat PRIMARY KEY(Id)\n" +
					");";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.err.println("Failed to Execute  scriptCreation. The error is " + e.getMessage());
		}

		//Ajouter une données dans la base de données
		String query = "INSERT INTO Ligue (nomLigue) VALUES ('Quidditch')";
		Statement stmt = con.createStatement();
		stmt.executeUpdate(query);

		query = "INSERT INTO Equipe (nomEquipe, NbreJoueurMax, nomLigue) VALUES ('Griffondor',10,'Quidditch')";
		stmt.executeUpdate(query);

		query = "INSERT INTO Participant (matricule, age, nom, prenom) VALUES ('05947254',12,'Malfois','Drago')";
		stmt.executeUpdate(query);

		query = "INSERT INTO Joueur (nomEquipe, matricule) VALUES ('Griffondor','05947254')";
	}

	@After
	public void tearDown() throws Exception {

		//Destruction des tables dans la base de données
		try {


			String query = "DROP TABLE Participant CASCADE;\n" +
					"DROP TABLE Equipe CASCADE;\n" +
					"DROP TABLE Joueur CASCADE;\n" +
					"DROP TABLE Ligue CASCADE;\n" +
					"DROP TABLE Resultat CASCADE;";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.err.println("Failed to Executes criptdestruction. The error is " + e.getMessage());
		}
		con.close();
	}

	@Test
	public void test() throws Exception{

		//Simulation des commandes dans l'application
		String input = "supprimerLigue Quidditch";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		//Démarrage de l'application
		Devoir2 devoir = new Devoir2();
		String args[] = {"local", "//postgres:5432/postgres", "postgres", ""};
		try{
			devoir.main(args);
		}catch(Exception e){
			throw e;
		}

		//Execution d'une commande SELECT dans la base de données
		PreparedStatement stmtAfficherLigue= con.prepareStatement("SELECT matricule FROM Joueur");
		ResultSet rset = stmtAfficherLigue.executeQuery();

		if(rset.next())
			fail("Échec, joueur non supprimée.");
		else
			System.out.println("Test réussi! (Unitaire: Supprimer joueur)");
	}
}
