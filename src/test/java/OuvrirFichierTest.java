import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class OuvrirFichierTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception  {
		BufferedReader buff;
		Devoir2 devoir = new Devoir2();
		String args[] = {"test", "test","test","test","tp2.dat"};
		try{
			buff = devoir.ouvrirFichier(args);
		}catch(Exception e){
			throw e;
		}
		assertEquals(buff.read(), new BufferedReader(new InputStreamReader(new FileInputStream(args[4]))).read());
		System.out.println("Test réussi: ouvrir fichier");
	}

}
