import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GetConnectionTest {
	Connexion con;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		con = new Connexion("local","//postgres:5432/postgres","postgres","postgres");
	}

	@After
	public void tearDown() throws Exception {
		con.fermer();
	}

	@Test
	public void test() throws Exception{
		assertNotNull(con.getConnection());
		System.out.println("Test réussi, connection non-nulle.");
	}
}
