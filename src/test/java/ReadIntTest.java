import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.StringTokenizer;

public class ReadIntTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		Devoir2 devoir = new Devoir2();
		int result;
		StringTokenizer token = new StringTokenizer("1111");
		try{
			result = devoir.readInt(token);
		}catch(Exception e){
			throw e;
		}
		assertEquals(1111,result);
		System.out.println("Test réussi: readint");
	}
}
