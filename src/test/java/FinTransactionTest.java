import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FinTransactionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Devoir2 devoir = new Devoir2();
		Boolean result;
		try{
			result = devoir.finTransaction("quitter");
		}catch(Exception e){
			throw e;
		}
		assertTrue(result);
		System.out.println("Test réussi: fin transaction");
	}
}
