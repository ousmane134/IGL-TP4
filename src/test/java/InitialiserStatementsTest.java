import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.PreparedStatement;

public class InitialiserStatementsTest {
	Connexion cx;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		cx = new Connexion("local","//postgres:5432/postgres","postgres","");
	}

	@After
	public void tearDown() throws Exception {
		cx.fermer();
	}

	@Test
	public void test() throws Exception{
		PreparedStatement stmtExisteParticipant = cx.getConnection().prepareStatement("SELECT matricule, age, nom, prenom FROM Participant WHERE matricule = ?");
		PreparedStatement stmtAjoutParticipant = cx.getConnection().prepareStatement("INSERT INTO Participant (matricule, age, nom, prenom) VALUES (?,?,?,?)");
		PreparedStatement stmtSupprimerParticipant = cx.getConnection().prepareStatement("DELETE FROM Participant WHERE matricule = ?");
		PreparedStatement stmtExisteJoueur2 = cx.getConnection().prepareStatement("SELECT nomEquipe, matricule FROM Joueur WHERE matricule = ?");

		PreparedStatement stmtExisteJoueur = cx.getConnection().prepareStatement("SELECT nomEquipe, matricule FROM Joueur WHERE nomEquipe = ? AND matricule = ?");
		PreparedStatement stmtAjoutJoueur = cx.getConnection().prepareStatement("INSERT INTO Joueur (nomEquipe, matricule) VALUES (?,?)");
		PreparedStatement stmtSupprimerJoueur = cx.getConnection().prepareStatement("DELETE FROM Joueur WHERE nomEquipe = ? AND matricule = ?");
		PreparedStatement stmtAfficherEquipe = cx.getConnection().prepareStatement("SELECT * FROM Joueur WHERE nomEquipe = ?");

		PreparedStatement stmtExisteEquipe = cx.getConnection().prepareStatement("SELECT nomEquipe, NbreJoueurMax, nomLigue FROM Equipe WHERE nomEquipe = ?");
		PreparedStatement stmtAjoutEquipe = cx.getConnection().prepareStatement("INSERT INTO Equipe (nomEquipe, NbreJoueurMax, nomLigue) VALUES (?,?,?)");
		PreparedStatement stmtAfficherEquipes = cx.getConnection().prepareStatement("SELECT * FROM Equipe");
		PreparedStatement stmtSupprimerEquipes = cx.getConnection().prepareStatement("DELETE FROM Equipe WHERE nomLigue = ?");

		PreparedStatement stmtExisteLigue = cx.getConnection().prepareStatement("SELECT nomLigue FROM Ligue WHERE nomLigue = ?");
		PreparedStatement stmtAjoutLigue = cx.getConnection().prepareStatement("INSERT INTO Ligue (nomLigue) VALUES (?)");
		PreparedStatement stmtAfficherLigue = cx.getConnection().prepareStatement("SELECT * FROM Equipe WHERE nomLigue = ?");
		PreparedStatement stmtSupprimerLigue= cx.getConnection().prepareStatement("DELETE FROM Ligue WHERE nomLigue = ?");
		PreparedStatement stmtJoueurEquipe = cx.getConnection().prepareStatement("SELECT Joueur.matricule FROM Equipe JOIN Joueur on (Equipe.nomEquipe = Joueur.nomEquipe) WHERE Equipe.nomLigue = ?");

		PreparedStatement stmtExisteResultat = cx.getConnection().prepareStatement("SELECT Id, scoreEquipeA, scoreEquipeB, nomEquipeA, nomEquipeB FROM Resultat WHERE Id = ?" );
		PreparedStatement stmtAjoutResultat = cx.getConnection().prepareStatement("INSERT INTO Resultat (Id, scoreEquipeA, scoreEquipeB, nomEquipeA, nomEquipeB) VALUES (?,?,?,?,?)");

		System.out.println("Test réussi, initialiser statements.");
	}

}
