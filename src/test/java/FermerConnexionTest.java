import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FermerConnexionTest {
	Connexion con;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		con = new Connexion("local","//postgres:5432/postgres","postgres","");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception{
		con.fermer();
		assertTrue(con.getConnection().isClosed());
		System.out.println("Test réussi, connection fermée.");
	}
}
